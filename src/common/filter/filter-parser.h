/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_FILTER_PARSER_H_INCLUDED
# define YY_YY_FILTER_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    CHARACTER_CONSTANT_START = 258, /* CHARACTER_CONSTANT_START  */
    SQUOTE = 259,                  /* SQUOTE  */
    STRING_LITERAL_START = 260,    /* STRING_LITERAL_START  */
    DQUOTE = 261,                  /* DQUOTE  */
    ESCSEQ = 262,                  /* ESCSEQ  */
    CHAR_STRING_TOKEN = 263,       /* CHAR_STRING_TOKEN  */
    DECIMAL_CONSTANT = 264,        /* DECIMAL_CONSTANT  */
    OCTAL_CONSTANT = 265,          /* OCTAL_CONSTANT  */
    HEXADECIMAL_CONSTANT = 266,    /* HEXADECIMAL_CONSTANT  */
    FLOAT_CONSTANT = 267,          /* FLOAT_CONSTANT  */
    LSBRAC = 268,                  /* LSBRAC  */
    RSBRAC = 269,                  /* RSBRAC  */
    LPAREN = 270,                  /* LPAREN  */
    RPAREN = 271,                  /* RPAREN  */
    LBRAC = 272,                   /* LBRAC  */
    RBRAC = 273,                   /* RBRAC  */
    RARROW = 274,                  /* RARROW  */
    STAR = 275,                    /* STAR  */
    PLUS = 276,                    /* PLUS  */
    MINUS = 277,                   /* MINUS  */
    MOD_OP = 278,                  /* MOD_OP  */
    DIV_OP = 279,                  /* DIV_OP  */
    RIGHT_OP = 280,                /* RIGHT_OP  */
    LEFT_OP = 281,                 /* LEFT_OP  */
    EQ_OP = 282,                   /* EQ_OP  */
    NE_OP = 283,                   /* NE_OP  */
    LE_OP = 284,                   /* LE_OP  */
    GE_OP = 285,                   /* GE_OP  */
    LT_OP = 286,                   /* LT_OP  */
    GT_OP = 287,                   /* GT_OP  */
    AND_OP = 288,                  /* AND_OP  */
    OR_OP = 289,                   /* OR_OP  */
    NOT_OP = 290,                  /* NOT_OP  */
    ASSIGN = 291,                  /* ASSIGN  */
    COLON = 292,                   /* COLON  */
    SEMICOLON = 293,               /* SEMICOLON  */
    DOTDOTDOT = 294,               /* DOTDOTDOT  */
    DOT = 295,                     /* DOT  */
    EQUAL = 296,                   /* EQUAL  */
    COMMA = 297,                   /* COMMA  */
    XOR_BIN = 298,                 /* XOR_BIN  */
    AND_BIN = 299,                 /* AND_BIN  */
    OR_BIN = 300,                  /* OR_BIN  */
    NOT_BIN = 301,                 /* NOT_BIN  */
    IDENTIFIER = 302,              /* IDENTIFIER  */
    GLOBAL_IDENTIFIER = 303,       /* GLOBAL_IDENTIFIER  */
    ERROR = 304                    /* ERROR  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define CHARACTER_CONSTANT_START 258
#define SQUOTE 259
#define STRING_LITERAL_START 260
#define DQUOTE 261
#define ESCSEQ 262
#define CHAR_STRING_TOKEN 263
#define DECIMAL_CONSTANT 264
#define OCTAL_CONSTANT 265
#define HEXADECIMAL_CONSTANT 266
#define FLOAT_CONSTANT 267
#define LSBRAC 268
#define RSBRAC 269
#define LPAREN 270
#define RPAREN 271
#define LBRAC 272
#define RBRAC 273
#define RARROW 274
#define STAR 275
#define PLUS 276
#define MINUS 277
#define MOD_OP 278
#define DIV_OP 279
#define RIGHT_OP 280
#define LEFT_OP 281
#define EQ_OP 282
#define NE_OP 283
#define LE_OP 284
#define GE_OP 285
#define LT_OP 286
#define GT_OP 287
#define AND_OP 288
#define OR_OP 289
#define NOT_OP 290
#define ASSIGN 291
#define COLON 292
#define SEMICOLON 293
#define DOTDOTDOT 294
#define DOT 295
#define EQUAL 296
#define COMMA 297
#define XOR_BIN 298
#define AND_BIN 299
#define OR_BIN 300
#define NOT_BIN 301
#define IDENTIFIER 302
#define GLOBAL_IDENTIFIER 303
#define ERROR 304

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 458 "filter-parser.y"

	long long ll;
	char c;
	struct gc_string *gs;
	struct filter_node *n;

#line 172 "filter-parser.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif




int yyparse (struct filter_parser_ctx *parser_ctx, yyscan_t scanner);

/* "%code provides" blocks.  */
#line 432 "filter-parser.y"

#include "common/macros.h"

LTTNG_HIDDEN
void setstring(struct filter_parser_ctx *parser_ctx, YYSTYPE *lvalp, const char *src);

#line 193 "filter-parser.h"

#endif /* !YY_YY_FILTER_PARSER_H_INCLUDED  */
