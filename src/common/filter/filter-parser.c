/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "filter-parser.y"

/*
 * filter-parser.y
 *
 * LTTng filter expression parser
 *
 * Copyright 2012 Mathieu Desnoyers <mathieu.desnoyers@efficios.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-only
 *
 * Grammar inspired from http://www.quut.com/c/ANSI-C-grammar-y.html
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <inttypes.h>
#include "common/bytecode/bytecode.h"
#include "filter-ast.h"
#include "filter-parser.h"
#include "memstream.h"

#include <common/compat/errno.h>
#include <common/macros.h>

#define WIDTH_u64_SCANF_IS_A_BROKEN_API	"20"
#define WIDTH_o64_SCANF_IS_A_BROKEN_API	"22"
#define WIDTH_x64_SCANF_IS_A_BROKEN_API	"17"
#define WIDTH_lg_SCANF_IS_A_BROKEN_API	"4096"	/* Hugely optimistic approximation */

#ifdef DEBUG
static const int print_xml = 1;
#define dbg_printf(fmt, args...)	\
	printf("[debug filter_parser] " fmt, ## args)
#else
static const int print_xml = 0;
#define dbg_printf(fmt, args...)				\
do {								\
	/* do nothing but check printf format */		\
	if (0)							\
		printf("[debug filter_parser] " fmt, ## args);	\
} while (0)
#endif

LTTNG_HIDDEN
int yydebug;
LTTNG_HIDDEN
int filter_parser_debug = 0;

LTTNG_HIDDEN
int yyparse(struct filter_parser_ctx *parser_ctx, yyscan_t scanner);
LTTNG_HIDDEN
int yylex(union YYSTYPE *yyval, yyscan_t scanner);
LTTNG_HIDDEN
int yylex_init_extra(struct filter_parser_ctx *parser_ctx, yyscan_t * ptr_yy_globals);
LTTNG_HIDDEN
int yylex_destroy(yyscan_t yyparser_ctx);
LTTNG_HIDDEN
void yyrestart(FILE * in_str, yyscan_t parser_ctx);

struct gc_string {
	struct cds_list_head gc;
	size_t alloclen;
	char s[];
};

static const char *node_type_to_str[] = {
	[ NODE_UNKNOWN ] = "NODE_UNKNOWN",
	[ NODE_ROOT ] = "NODE_ROOT",
	[ NODE_EXPRESSION ] = "NODE_EXPRESSION",
	[ NODE_OP ] = "NODE_OP",
	[ NODE_UNARY_OP ] = "NODE_UNARY_OP",
};

LTTNG_HIDDEN
const char *node_type(struct filter_node *node)
{
	if (node->type < NR_NODE_TYPES)
		return node_type_to_str[node->type];
	else
		return NULL;
}

static struct gc_string *gc_string_alloc(struct filter_parser_ctx *parser_ctx,
					 size_t len)
{
	struct gc_string *gstr;
	size_t alloclen;

	/* TODO: could be faster with find first bit or glib Gstring */
	/* sizeof long to account for malloc header (int or long ?) */
	for (alloclen = 8; alloclen < sizeof(long) + sizeof(*gstr) + len;
	     alloclen *= 2);

	gstr = zmalloc(alloclen);
	if (!gstr) {
		goto end;
	}
	cds_list_add(&gstr->gc, &parser_ctx->allocated_strings);
	gstr->alloclen = alloclen;
end:
	return gstr;
}

/*
 * note: never use gc_string_append on a string that has external references.
 * gsrc will be garbage collected immediately, and gstr might be.
 * Should only be used to append characters to a string literal or constant.
 */
static
struct gc_string *gc_string_append(struct filter_parser_ctx *parser_ctx,
				   struct gc_string *gstr,
				   struct gc_string *gsrc)
{
	size_t newlen = strlen(gsrc->s) + strlen(gstr->s) + 1;
	size_t alloclen;

	/* TODO: could be faster with find first bit or glib Gstring */
	/* sizeof long to account for malloc header (int or long ?) */
	for (alloclen = 8; alloclen < sizeof(long) + sizeof(*gstr) + newlen;
	     alloclen *= 2);

	if (alloclen > gstr->alloclen) {
		struct gc_string *newgstr;

		newgstr = gc_string_alloc(parser_ctx, newlen);
		strcpy(newgstr->s, gstr->s);
		strcat(newgstr->s, gsrc->s);
		cds_list_del(&gstr->gc);
		free(gstr);
		gstr = newgstr;
	} else {
		strcat(gstr->s, gsrc->s);
	}
	cds_list_del(&gsrc->gc);
	free(gsrc);
	return gstr;
}

LTTNG_HIDDEN
void setstring(struct filter_parser_ctx *parser_ctx, YYSTYPE *lvalp, const char *src)
{
	lvalp->gs = gc_string_alloc(parser_ctx, strlen(src) + 1);
	strcpy(lvalp->gs->s, src);
}

static struct filter_node *make_node(struct filter_parser_ctx *scanner,
				  enum node_type type)
{
	struct filter_ast *ast = filter_parser_get_ast(scanner);
	struct filter_node *node;

	node = zmalloc(sizeof(*node));
	if (!node)
		return NULL;
	memset(node, 0, sizeof(*node));
	node->type = type;
	cds_list_add(&node->gc, &ast->allocated_nodes);

	switch (type) {
	case NODE_ROOT:
		fprintf(stderr, "[error] %s: trying to create root node\n", __func__);
		break;

	case NODE_EXPRESSION:
		break;
	case NODE_OP:
		break;
	case NODE_UNARY_OP:
		break;

	case NODE_UNKNOWN:
	default:
		fprintf(stderr, "[error] %s: unknown node type %d\n", __func__,
			(int) type);
		break;
	}

	return node;
}

static struct filter_node *make_op_node(struct filter_parser_ctx *scanner,
			enum op_type type,
			struct filter_node *lchild,
			struct filter_node *rchild)
{
	struct filter_ast *ast = filter_parser_get_ast(scanner);
	struct filter_node *node;

	node = zmalloc(sizeof(*node));
	if (!node)
		return NULL;
	memset(node, 0, sizeof(*node));
	node->type = NODE_OP;
	cds_list_add(&node->gc, &ast->allocated_nodes);
	node->u.op.type = type;
	node->u.op.lchild = lchild;
	node->u.op.rchild = rchild;
	return node;
}

static
void yyerror(struct filter_parser_ctx *parser_ctx, yyscan_t scanner, const char *str)
{
	fprintf(stderr, "error %s\n", str);
}

#define parse_error(parser_ctx, str)				\
do {								\
	yyerror(parser_ctx, parser_ctx->scanner, YY_("parse error: " str "\n"));	\
	YYERROR;						\
} while (0)

static void free_strings(struct cds_list_head *list)
{
	struct gc_string *gstr, *tmp;

	cds_list_for_each_entry_safe(gstr, tmp, list, gc)
		free(gstr);
}

static struct filter_ast *filter_ast_alloc(void)
{
	struct filter_ast *ast;

	ast = zmalloc(sizeof(*ast));
	if (!ast)
		return NULL;
	memset(ast, 0, sizeof(*ast));
	CDS_INIT_LIST_HEAD(&ast->allocated_nodes);
	ast->root.type = NODE_ROOT;
	return ast;
}

static void filter_ast_free(struct filter_ast *ast)
{
	struct filter_node *node, *tmp;

	cds_list_for_each_entry_safe(node, tmp, &ast->allocated_nodes, gc)
		free(node);
	free(ast);
}

LTTNG_HIDDEN
int filter_parser_ctx_append_ast(struct filter_parser_ctx *parser_ctx)
{
	return yyparse(parser_ctx, parser_ctx->scanner);
}

LTTNG_HIDDEN
struct filter_parser_ctx *filter_parser_ctx_alloc(FILE *input)
{
	struct filter_parser_ctx *parser_ctx;
	int ret;

	yydebug = filter_parser_debug;

	parser_ctx = zmalloc(sizeof(*parser_ctx));
	if (!parser_ctx)
		return NULL;
	memset(parser_ctx, 0, sizeof(*parser_ctx));

	ret = yylex_init_extra(parser_ctx, &parser_ctx->scanner);
	if (ret) {
		fprintf(stderr, "yylex_init error\n");
		goto cleanup_parser_ctx;
	}
	/* Start processing new stream */
	yyrestart(input, parser_ctx->scanner);

	parser_ctx->ast = filter_ast_alloc();
	if (!parser_ctx->ast)
		goto cleanup_lexer;
	CDS_INIT_LIST_HEAD(&parser_ctx->allocated_strings);

	if (yydebug)
		fprintf(stdout, "parser_ctx input is a%s.\n",
			isatty(fileno(input)) ? "n interactive tty" :
						" noninteractive file");

	return parser_ctx;

cleanup_lexer:
	ret = yylex_destroy(parser_ctx->scanner);
	if (!ret)
		fprintf(stderr, "yylex_destroy error\n");
cleanup_parser_ctx:
	free(parser_ctx);
	return NULL;
}

LTTNG_HIDDEN
void filter_parser_ctx_free(struct filter_parser_ctx *parser_ctx)
{
	int ret;

	ret = yylex_destroy(parser_ctx->scanner);
	if (ret)
		fprintf(stderr, "yylex_destroy error\n");

	filter_ast_free(parser_ctx->ast);
	free_strings(&parser_ctx->allocated_strings);
	filter_ir_free(parser_ctx);
	free(parser_ctx->bytecode);
	free(parser_ctx->bytecode_reloc);

	free(parser_ctx);
}

LTTNG_HIDDEN
int filter_parser_ctx_create_from_filter_expression(
		const char *filter_expression, struct filter_parser_ctx **ctxp)
{
	int ret;
	struct filter_parser_ctx *ctx = NULL;
	FILE *fmem = NULL;

	assert(filter_expression);
	assert(ctxp);

	/*
	 * Casting const to non-const, as the underlying function will use it in
	 * read-only mode.
	 */
	fmem = lttng_fmemopen((void *) filter_expression,
			strlen(filter_expression), "r");
	if (!fmem) {
		fprintf(stderr, "Error opening memory as stream\n");
		ret = -LTTNG_ERR_FILTER_NOMEM;
		goto error;
	}
	ctx = filter_parser_ctx_alloc(fmem);
	if (!ctx) {
		fprintf(stderr, "Error allocating parser\n");
		ret = -LTTNG_ERR_FILTER_NOMEM;
		goto filter_alloc_error;
	}
	ret = filter_parser_ctx_append_ast(ctx);
	if (ret) {
		fprintf(stderr, "Parse error\n");
		ret = -LTTNG_ERR_FILTER_INVAL;
		goto parse_error;
	}
	if (print_xml) {
		ret = filter_visitor_print_xml(ctx, stdout, 0);
		if (ret) {
			fflush(stdout);
			fprintf(stderr, "XML print error\n");
			ret = -LTTNG_ERR_FILTER_INVAL;
			goto parse_error;
		}
	}

	dbg_printf("Generating IR... ");
	fflush(stdout);
	ret = filter_visitor_ir_generate(ctx);
	if (ret) {
		fprintf(stderr, "Generate IR error\n");
		ret = -LTTNG_ERR_FILTER_INVAL;
		goto parse_error;
	}
	dbg_printf("done\n");

	dbg_printf("Validating IR... ");
	fflush(stdout);
	ret = filter_visitor_ir_check_binary_op_nesting(ctx);
	if (ret) {
		ret = -LTTNG_ERR_FILTER_INVAL;
		goto parse_error;
	}

	/* Normalize globbing patterns in the expression. */
	ret = filter_visitor_ir_normalize_glob_patterns(ctx);
	if (ret) {
		ret = -LTTNG_ERR_FILTER_INVAL;
		goto parse_error;
	}

	/* Validate strings used as literals in the expression. */
	ret = filter_visitor_ir_validate_string(ctx);
	if (ret) {
		ret = -LTTNG_ERR_FILTER_INVAL;
		goto parse_error;
	}

	/* Validate globbing patterns in the expression. */
	ret = filter_visitor_ir_validate_globbing(ctx);
	if (ret) {
		ret = -LTTNG_ERR_FILTER_INVAL;
		goto parse_error;
	}

	dbg_printf("done\n");

	dbg_printf("Generating bytecode... ");
	fflush(stdout);
	ret = filter_visitor_bytecode_generate(ctx);
	if (ret) {
		fprintf(stderr, "Generate bytecode error\n");
		ret = -LTTNG_ERR_FILTER_INVAL;
		goto parse_error;
	}
	dbg_printf("done\n");
	dbg_printf("Size of bytecode generated: %u bytes.\n",
			bytecode_get_len(&ctx->bytecode->b));

	/* No need to keep the memory stream. */
	if (fclose(fmem) != 0) {
		fprintf(stderr, "fclose (%d) \n", errno);
		ret = -LTTNG_ERR_FILTER_INVAL;
	}

	*ctxp = ctx;
	return 0;

parse_error:
	filter_ir_free(ctx);
	filter_parser_ctx_free(ctx);
filter_alloc_error:
	if (fclose(fmem) != 0) {
		fprintf(stderr, "fclose (%d) \n", errno);
	}
error:
	return ret;
}


#line 501 "filter-parser.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_FILTER_PARSER_H_INCLUDED
# define YY_YY_FILTER_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    CHARACTER_CONSTANT_START = 258, /* CHARACTER_CONSTANT_START  */
    SQUOTE = 259,                  /* SQUOTE  */
    STRING_LITERAL_START = 260,    /* STRING_LITERAL_START  */
    DQUOTE = 261,                  /* DQUOTE  */
    ESCSEQ = 262,                  /* ESCSEQ  */
    CHAR_STRING_TOKEN = 263,       /* CHAR_STRING_TOKEN  */
    DECIMAL_CONSTANT = 264,        /* DECIMAL_CONSTANT  */
    OCTAL_CONSTANT = 265,          /* OCTAL_CONSTANT  */
    HEXADECIMAL_CONSTANT = 266,    /* HEXADECIMAL_CONSTANT  */
    FLOAT_CONSTANT = 267,          /* FLOAT_CONSTANT  */
    LSBRAC = 268,                  /* LSBRAC  */
    RSBRAC = 269,                  /* RSBRAC  */
    LPAREN = 270,                  /* LPAREN  */
    RPAREN = 271,                  /* RPAREN  */
    LBRAC = 272,                   /* LBRAC  */
    RBRAC = 273,                   /* RBRAC  */
    RARROW = 274,                  /* RARROW  */
    STAR = 275,                    /* STAR  */
    PLUS = 276,                    /* PLUS  */
    MINUS = 277,                   /* MINUS  */
    MOD_OP = 278,                  /* MOD_OP  */
    DIV_OP = 279,                  /* DIV_OP  */
    RIGHT_OP = 280,                /* RIGHT_OP  */
    LEFT_OP = 281,                 /* LEFT_OP  */
    EQ_OP = 282,                   /* EQ_OP  */
    NE_OP = 283,                   /* NE_OP  */
    LE_OP = 284,                   /* LE_OP  */
    GE_OP = 285,                   /* GE_OP  */
    LT_OP = 286,                   /* LT_OP  */
    GT_OP = 287,                   /* GT_OP  */
    AND_OP = 288,                  /* AND_OP  */
    OR_OP = 289,                   /* OR_OP  */
    NOT_OP = 290,                  /* NOT_OP  */
    ASSIGN = 291,                  /* ASSIGN  */
    COLON = 292,                   /* COLON  */
    SEMICOLON = 293,               /* SEMICOLON  */
    DOTDOTDOT = 294,               /* DOTDOTDOT  */
    DOT = 295,                     /* DOT  */
    EQUAL = 296,                   /* EQUAL  */
    COMMA = 297,                   /* COMMA  */
    XOR_BIN = 298,                 /* XOR_BIN  */
    AND_BIN = 299,                 /* AND_BIN  */
    OR_BIN = 300,                  /* OR_BIN  */
    NOT_BIN = 301,                 /* NOT_BIN  */
    IDENTIFIER = 302,              /* IDENTIFIER  */
    GLOBAL_IDENTIFIER = 303,       /* GLOBAL_IDENTIFIER  */
    ERROR = 304                    /* ERROR  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define CHARACTER_CONSTANT_START 258
#define SQUOTE 259
#define STRING_LITERAL_START 260
#define DQUOTE 261
#define ESCSEQ 262
#define CHAR_STRING_TOKEN 263
#define DECIMAL_CONSTANT 264
#define OCTAL_CONSTANT 265
#define HEXADECIMAL_CONSTANT 266
#define FLOAT_CONSTANT 267
#define LSBRAC 268
#define RSBRAC 269
#define LPAREN 270
#define RPAREN 271
#define LBRAC 272
#define RBRAC 273
#define RARROW 274
#define STAR 275
#define PLUS 276
#define MINUS 277
#define MOD_OP 278
#define DIV_OP 279
#define RIGHT_OP 280
#define LEFT_OP 281
#define EQ_OP 282
#define NE_OP 283
#define LE_OP 284
#define GE_OP 285
#define LT_OP 286
#define GT_OP 287
#define AND_OP 288
#define OR_OP 289
#define NOT_OP 290
#define ASSIGN 291
#define COLON 292
#define SEMICOLON 293
#define DOTDOTDOT 294
#define DOT 295
#define EQUAL 296
#define COMMA 297
#define XOR_BIN 298
#define AND_BIN 299
#define OR_BIN 300
#define NOT_BIN 301
#define IDENTIFIER 302
#define GLOBAL_IDENTIFIER 303
#define ERROR 304

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 458 "filter-parser.y"

	long long ll;
	char c;
	struct gc_string *gs;
	struct filter_node *n;

#line 659 "filter-parser.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif




int yyparse (struct filter_parser_ctx *parser_ctx, yyscan_t scanner);

/* "%code provides" blocks.  */
#line 432 "filter-parser.y"

#include "common/macros.h"

LTTNG_HIDDEN
void setstring(struct filter_parser_ctx *parser_ctx, YYSTYPE *lvalp, const char *src);

#line 680 "filter-parser.c"

#endif /* !YY_YY_FILTER_PARSER_H_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_CHARACTER_CONSTANT_START = 3,   /* CHARACTER_CONSTANT_START  */
  YYSYMBOL_SQUOTE = 4,                     /* SQUOTE  */
  YYSYMBOL_STRING_LITERAL_START = 5,       /* STRING_LITERAL_START  */
  YYSYMBOL_DQUOTE = 6,                     /* DQUOTE  */
  YYSYMBOL_ESCSEQ = 7,                     /* ESCSEQ  */
  YYSYMBOL_CHAR_STRING_TOKEN = 8,          /* CHAR_STRING_TOKEN  */
  YYSYMBOL_DECIMAL_CONSTANT = 9,           /* DECIMAL_CONSTANT  */
  YYSYMBOL_OCTAL_CONSTANT = 10,            /* OCTAL_CONSTANT  */
  YYSYMBOL_HEXADECIMAL_CONSTANT = 11,      /* HEXADECIMAL_CONSTANT  */
  YYSYMBOL_FLOAT_CONSTANT = 12,            /* FLOAT_CONSTANT  */
  YYSYMBOL_LSBRAC = 13,                    /* LSBRAC  */
  YYSYMBOL_RSBRAC = 14,                    /* RSBRAC  */
  YYSYMBOL_LPAREN = 15,                    /* LPAREN  */
  YYSYMBOL_RPAREN = 16,                    /* RPAREN  */
  YYSYMBOL_LBRAC = 17,                     /* LBRAC  */
  YYSYMBOL_RBRAC = 18,                     /* RBRAC  */
  YYSYMBOL_RARROW = 19,                    /* RARROW  */
  YYSYMBOL_STAR = 20,                      /* STAR  */
  YYSYMBOL_PLUS = 21,                      /* PLUS  */
  YYSYMBOL_MINUS = 22,                     /* MINUS  */
  YYSYMBOL_MOD_OP = 23,                    /* MOD_OP  */
  YYSYMBOL_DIV_OP = 24,                    /* DIV_OP  */
  YYSYMBOL_RIGHT_OP = 25,                  /* RIGHT_OP  */
  YYSYMBOL_LEFT_OP = 26,                   /* LEFT_OP  */
  YYSYMBOL_EQ_OP = 27,                     /* EQ_OP  */
  YYSYMBOL_NE_OP = 28,                     /* NE_OP  */
  YYSYMBOL_LE_OP = 29,                     /* LE_OP  */
  YYSYMBOL_GE_OP = 30,                     /* GE_OP  */
  YYSYMBOL_LT_OP = 31,                     /* LT_OP  */
  YYSYMBOL_GT_OP = 32,                     /* GT_OP  */
  YYSYMBOL_AND_OP = 33,                    /* AND_OP  */
  YYSYMBOL_OR_OP = 34,                     /* OR_OP  */
  YYSYMBOL_NOT_OP = 35,                    /* NOT_OP  */
  YYSYMBOL_ASSIGN = 36,                    /* ASSIGN  */
  YYSYMBOL_COLON = 37,                     /* COLON  */
  YYSYMBOL_SEMICOLON = 38,                 /* SEMICOLON  */
  YYSYMBOL_DOTDOTDOT = 39,                 /* DOTDOTDOT  */
  YYSYMBOL_DOT = 40,                       /* DOT  */
  YYSYMBOL_EQUAL = 41,                     /* EQUAL  */
  YYSYMBOL_COMMA = 42,                     /* COMMA  */
  YYSYMBOL_XOR_BIN = 43,                   /* XOR_BIN  */
  YYSYMBOL_AND_BIN = 44,                   /* AND_BIN  */
  YYSYMBOL_OR_BIN = 45,                    /* OR_BIN  */
  YYSYMBOL_NOT_BIN = 46,                   /* NOT_BIN  */
  YYSYMBOL_IDENTIFIER = 47,                /* IDENTIFIER  */
  YYSYMBOL_GLOBAL_IDENTIFIER = 48,         /* GLOBAL_IDENTIFIER  */
  YYSYMBOL_ERROR = 49,                     /* ERROR  */
  YYSYMBOL_YYACCEPT = 50,                  /* $accept  */
  YYSYMBOL_c_char_sequence = 51,           /* c_char_sequence  */
  YYSYMBOL_c_char = 52,                    /* c_char  */
  YYSYMBOL_s_char_sequence = 53,           /* s_char_sequence  */
  YYSYMBOL_s_char = 54,                    /* s_char  */
  YYSYMBOL_primary_expression = 55,        /* primary_expression  */
  YYSYMBOL_identifiers = 56,               /* identifiers  */
  YYSYMBOL_prefix_expression_rec = 57,     /* prefix_expression_rec  */
  YYSYMBOL_prefix_expression = 58,         /* prefix_expression  */
  YYSYMBOL_postfix_expression = 59,        /* postfix_expression  */
  YYSYMBOL_unary_expression = 60,          /* unary_expression  */
  YYSYMBOL_unary_operator = 61,            /* unary_operator  */
  YYSYMBOL_multiplicative_expression = 62, /* multiplicative_expression  */
  YYSYMBOL_additive_expression = 63,       /* additive_expression  */
  YYSYMBOL_shift_expression = 64,          /* shift_expression  */
  YYSYMBOL_and_expression = 65,            /* and_expression  */
  YYSYMBOL_exclusive_or_expression = 66,   /* exclusive_or_expression  */
  YYSYMBOL_inclusive_or_expression = 67,   /* inclusive_or_expression  */
  YYSYMBOL_relational_expression = 68,     /* relational_expression  */
  YYSYMBOL_equality_expression = 69,       /* equality_expression  */
  YYSYMBOL_logical_and_expression = 70,    /* logical_and_expression  */
  YYSYMBOL_logical_or_expression = 71,     /* logical_or_expression  */
  YYSYMBOL_expression = 72,                /* expression  */
  YYSYMBOL_translation_unit = 73           /* translation_unit  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  65
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   77

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  50
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  24
/* YYNRULES -- Number of rules.  */
#define YYNRULES  63
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  94

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   304


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   492,   492,   494,   499,   501,   510,   512,   517,   519,
     526,   535,   546,   555,   564,   570,   576,   582,   591,   597,
     606,   610,   619,   623,   632,   636,   642,   651,   653,   655,
     663,   668,   673,   678,   686,   688,   692,   696,   703,   705,
     709,   716,   718,   722,   729,   731,   738,   740,   747,   749,
     756,   758,   762,   766,   770,   777,   779,   783,   790,   792,
     799,   801,   808,   813
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"",
  "CHARACTER_CONSTANT_START", "SQUOTE", "STRING_LITERAL_START", "DQUOTE",
  "ESCSEQ", "CHAR_STRING_TOKEN", "DECIMAL_CONSTANT", "OCTAL_CONSTANT",
  "HEXADECIMAL_CONSTANT", "FLOAT_CONSTANT", "LSBRAC", "RSBRAC", "LPAREN",
  "RPAREN", "LBRAC", "RBRAC", "RARROW", "STAR", "PLUS", "MINUS", "MOD_OP",
  "DIV_OP", "RIGHT_OP", "LEFT_OP", "EQ_OP", "NE_OP", "LE_OP", "GE_OP",
  "LT_OP", "GT_OP", "AND_OP", "OR_OP", "NOT_OP", "ASSIGN", "COLON",
  "SEMICOLON", "DOTDOTDOT", "DOT", "EQUAL", "COMMA", "XOR_BIN", "AND_BIN",
  "OR_BIN", "NOT_BIN", "IDENTIFIER", "GLOBAL_IDENTIFIER", "ERROR",
  "$accept", "c_char_sequence", "c_char", "s_char_sequence", "s_char",
  "primary_expression", "identifiers", "prefix_expression_rec",
  "prefix_expression", "postfix_expression", "unary_expression",
  "unary_operator", "multiplicative_expression", "additive_expression",
  "shift_expression", "and_expression", "exclusive_or_expression",
  "inclusive_or_expression", "relational_expression",
  "equality_expression", "logical_and_expression", "logical_or_expression",
  "expression", "translation_unit", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-42)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int8 yypact[] =
{
      -3,     8,    20,   -42,   -42,   -42,   -42,    -3,   -42,   -42,
     -42,   -42,   -42,   -42,   -42,    -8,   -42,   -15,   -42,    -3,
     -10,     1,    16,   -41,   -32,    18,     4,    22,    25,    17,
     -42,    64,   -42,   -42,    13,   -42,   -42,   -42,   -42,    40,
     -42,    49,    -3,   -42,     5,     5,   -42,    -3,    -3,    -3,
      -3,    -3,    -3,    -3,    -3,    -3,    -3,    -3,    -3,    -3,
      -3,    -3,    -3,    -3,    -3,   -42,   -42,   -42,   -42,   -42,
     -42,    52,   -42,   -42,   -42,   -42,   -42,   -10,   -10,     1,
       1,    16,   -41,   -32,    18,    18,    18,    18,     4,     4,
      22,    25,    -8,   -42
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       0,     0,     0,    10,    11,    12,    13,     0,    30,    31,
      32,    33,    18,    19,    28,    22,    24,    27,    34,     0,
      38,    41,    44,    46,    48,    50,    55,    58,    60,    62,
      63,     0,     5,     4,     0,     2,    14,     9,     8,     0,
       6,     0,     0,    23,     0,     0,    29,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     1,    16,     3,    15,     7,
      17,     0,    26,    25,    35,    37,    36,    39,    40,    43,
      42,    45,    47,    49,    53,    54,    51,    52,    56,    57,
      59,    61,    20,    21
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -42,   -42,    33,   -42,    29,   -42,   -42,   -23,    10,   -42,
     -18,   -42,     6,     7,    19,    15,    21,   -20,     0,     9,
      11,   -42,    67,   -42
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
       0,    34,    35,    39,    40,    14,    15,    43,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int8 yytable[] =
{
       1,    46,     2,    54,    44,    42,     3,     4,     5,     6,
      47,    55,     7,    48,    49,    32,    33,    66,     8,     9,
      32,    33,    50,    51,    71,    45,    36,    37,    38,    74,
      75,    76,    10,    57,    58,    59,    60,    84,    85,    86,
      87,    52,    53,    11,    12,    13,    68,    37,    38,    61,
      62,    64,    12,    13,    72,    73,    77,    78,    63,    79,
      80,    88,    89,    56,    65,    70,    92,    67,    69,    93,
      82,     0,    90,    81,    41,    91,     0,    83
};

static const yytype_int8 yycheck[] =
{
       3,    19,     5,    44,    19,    13,     9,    10,    11,    12,
      20,    43,    15,    23,    24,     7,     8,     4,    21,    22,
       7,     8,    21,    22,    42,    40,     6,     7,     8,    47,
      48,    49,    35,    29,    30,    31,    32,    57,    58,    59,
      60,    25,    26,    46,    47,    48,     6,     7,     8,    27,
      28,    34,    47,    48,    44,    45,    50,    51,    33,    52,
      53,    61,    62,    45,     0,    16,    14,    34,    39,    92,
      55,    -1,    63,    54,     7,    64,    -1,    56
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     3,     5,     9,    10,    11,    12,    15,    21,    22,
      35,    46,    47,    48,    55,    56,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,     7,     8,    51,    52,     6,     7,     8,    53,
      54,    72,    13,    57,    19,    40,    60,    20,    23,    24,
      21,    22,    25,    26,    44,    43,    45,    29,    30,    31,
      32,    27,    28,    33,    34,     0,     4,    52,     6,    54,
      16,    60,    58,    58,    60,    60,    60,    62,    62,    63,
      63,    64,    65,    66,    67,    67,    67,    67,    68,    68,
      69,    70,    14,    57
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    50,    51,    51,    52,    52,    53,    53,    54,    54,
      55,    55,    55,    55,    55,    55,    55,    55,    56,    56,
      57,    57,    58,    58,    59,    59,    59,    60,    60,    60,
      61,    61,    61,    61,    62,    62,    62,    62,    63,    63,
      63,    64,    64,    64,    65,    65,    66,    66,    67,    67,
      68,    68,    68,    68,    68,    69,    69,    69,    70,    70,
      71,    71,    72,    73
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     2,     1,     1,     1,     2,     1,     1,
       1,     1,     1,     1,     2,     3,     3,     3,     1,     1,
       3,     4,     1,     2,     1,     3,     3,     1,     1,     2,
       1,     1,     1,     1,     1,     3,     3,     3,     1,     3,
       3,     1,     3,     3,     1,     3,     1,     3,     1,     3,
       1,     3,     3,     3,     3,     1,     3,     3,     1,     3,
       1,     3,     1,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (parser_ctx, scanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, parser_ctx, scanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, struct filter_parser_ctx *parser_ctx, yyscan_t scanner)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (parser_ctx);
  YY_USE (scanner);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, struct filter_parser_ctx *parser_ctx, yyscan_t scanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep, parser_ctx, scanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule, struct filter_parser_ctx *parser_ctx, yyscan_t scanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)], parser_ctx, scanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule, parser_ctx, scanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, struct filter_parser_ctx *parser_ctx, yyscan_t scanner)
{
  YY_USE (yyvaluep);
  YY_USE (parser_ctx);
  YY_USE (scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (struct filter_parser_ctx *parser_ctx, yyscan_t scanner)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, scanner);
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* c_char_sequence: c_char  */
#line 493 "filter-parser.y"
                {	(yyval.gs) = (yyvsp[0].gs);					}
#line 1798 "filter-parser.c"
    break;

  case 3: /* c_char_sequence: c_char_sequence c_char  */
#line 495 "filter-parser.y"
                {	(yyval.gs) = gc_string_append(parser_ctx, (yyvsp[-1].gs), (yyvsp[0].gs));		}
#line 1804 "filter-parser.c"
    break;

  case 4: /* c_char: CHAR_STRING_TOKEN  */
#line 500 "filter-parser.y"
                {	(yyval.gs) = yylval.gs;					}
#line 1810 "filter-parser.c"
    break;

  case 5: /* c_char: ESCSEQ  */
#line 502 "filter-parser.y"
                {
			parse_error(parser_ctx, "escape sequences not supported yet");
		}
#line 1818 "filter-parser.c"
    break;

  case 6: /* s_char_sequence: s_char  */
#line 511 "filter-parser.y"
                {	(yyval.gs) = (yyvsp[0].gs);					}
#line 1824 "filter-parser.c"
    break;

  case 7: /* s_char_sequence: s_char_sequence s_char  */
#line 513 "filter-parser.y"
                {	(yyval.gs) = gc_string_append(parser_ctx, (yyvsp[-1].gs), (yyvsp[0].gs));		}
#line 1830 "filter-parser.c"
    break;

  case 8: /* s_char: CHAR_STRING_TOKEN  */
#line 518 "filter-parser.y"
                {	(yyval.gs) = yylval.gs;					}
#line 1836 "filter-parser.c"
    break;

  case 9: /* s_char: ESCSEQ  */
#line 520 "filter-parser.y"
                {
			parse_error(parser_ctx, "escape sequences not supported yet");
		}
#line 1844 "filter-parser.c"
    break;

  case 10: /* primary_expression: DECIMAL_CONSTANT  */
#line 527 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_EXPRESSION);
			(yyval.n)->u.expression.type = AST_EXP_CONSTANT;
			if (sscanf(yylval.gs->s, "%" WIDTH_u64_SCANF_IS_A_BROKEN_API SCNu64,
					&(yyval.n)->u.expression.u.constant) != 1) {
				parse_error(parser_ctx, "cannot scanf decimal constant");
			}
		}
#line 1857 "filter-parser.c"
    break;

  case 11: /* primary_expression: OCTAL_CONSTANT  */
#line 536 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_EXPRESSION);
			(yyval.n)->u.expression.type = AST_EXP_CONSTANT;
			if (!strcmp(yylval.gs->s, "0")) {
				(yyval.n)->u.expression.u.constant = 0;
			} else if (sscanf(yylval.gs->s, "0%" WIDTH_o64_SCANF_IS_A_BROKEN_API SCNo64,
					&(yyval.n)->u.expression.u.constant) != 1) {
				parse_error(parser_ctx, "cannot scanf octal constant");
			}
		}
#line 1872 "filter-parser.c"
    break;

  case 12: /* primary_expression: HEXADECIMAL_CONSTANT  */
#line 547 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_EXPRESSION);
			(yyval.n)->u.expression.type = AST_EXP_CONSTANT;
			if (sscanf(yylval.gs->s, "0x%" WIDTH_x64_SCANF_IS_A_BROKEN_API SCNx64,
					&(yyval.n)->u.expression.u.constant) != 1) {
				parse_error(parser_ctx, "cannot scanf hexadecimal constant");
			}
		}
#line 1885 "filter-parser.c"
    break;

  case 13: /* primary_expression: FLOAT_CONSTANT  */
#line 556 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_EXPRESSION);
			(yyval.n)->u.expression.type = AST_EXP_FLOAT_CONSTANT;
			if (sscanf(yylval.gs->s, "%" WIDTH_lg_SCANF_IS_A_BROKEN_API "lg",
					&(yyval.n)->u.expression.u.float_constant) != 1) {
				parse_error(parser_ctx, "cannot scanf float constant");
			}
		}
#line 1898 "filter-parser.c"
    break;

  case 14: /* primary_expression: STRING_LITERAL_START DQUOTE  */
#line 565 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_EXPRESSION);
			(yyval.n)->u.expression.type = AST_EXP_STRING;
			(yyval.n)->u.expression.u.string = "";
		}
#line 1908 "filter-parser.c"
    break;

  case 15: /* primary_expression: STRING_LITERAL_START s_char_sequence DQUOTE  */
#line 571 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_EXPRESSION);
			(yyval.n)->u.expression.type = AST_EXP_STRING;
			(yyval.n)->u.expression.u.string = (yyvsp[-1].gs)->s;
		}
#line 1918 "filter-parser.c"
    break;

  case 16: /* primary_expression: CHARACTER_CONSTANT_START c_char_sequence SQUOTE  */
#line 577 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_EXPRESSION);
			(yyval.n)->u.expression.type = AST_EXP_STRING;
			(yyval.n)->u.expression.u.string = (yyvsp[-1].gs)->s;
		}
#line 1928 "filter-parser.c"
    break;

  case 17: /* primary_expression: LPAREN expression RPAREN  */
#line 583 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_EXPRESSION);
			(yyval.n)->u.expression.type = AST_EXP_NESTED;
			(yyval.n)->u.expression.u.child = (yyvsp[-1].n);
		}
#line 1938 "filter-parser.c"
    break;

  case 18: /* identifiers: IDENTIFIER  */
#line 592 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_EXPRESSION);
			(yyval.n)->u.expression.type = AST_EXP_IDENTIFIER;
			(yyval.n)->u.expression.u.identifier = yylval.gs->s;
		}
#line 1948 "filter-parser.c"
    break;

  case 19: /* identifiers: GLOBAL_IDENTIFIER  */
#line 598 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_EXPRESSION);
			(yyval.n)->u.expression.type = AST_EXP_GLOBAL_IDENTIFIER;
			(yyval.n)->u.expression.u.identifier = yylval.gs->s;
		}
#line 1958 "filter-parser.c"
    break;

  case 20: /* prefix_expression_rec: LSBRAC unary_expression RSBRAC  */
#line 607 "filter-parser.y"
                {
			(yyval.n) = (yyvsp[-1].n);
		}
#line 1966 "filter-parser.c"
    break;

  case 21: /* prefix_expression_rec: LSBRAC unary_expression RSBRAC prefix_expression_rec  */
#line 611 "filter-parser.y"
                {
			(yyval.n) = (yyvsp[-2].n);
			(yyval.n)->u.expression.pre_op = AST_LINK_BRACKET;
			(yyval.n)->u.expression.prev = (yyvsp[0].n);
		}
#line 1976 "filter-parser.c"
    break;

  case 22: /* prefix_expression: identifiers  */
#line 620 "filter-parser.y"
                {
			(yyval.n) = (yyvsp[0].n);
		}
#line 1984 "filter-parser.c"
    break;

  case 23: /* prefix_expression: identifiers prefix_expression_rec  */
#line 624 "filter-parser.y"
                {
			(yyval.n) = (yyvsp[-1].n);
			(yyval.n)->u.expression.pre_op = AST_LINK_BRACKET;
			(yyval.n)->u.expression.next_bracket = (yyvsp[0].n);
		}
#line 1994 "filter-parser.c"
    break;

  case 24: /* postfix_expression: prefix_expression  */
#line 633 "filter-parser.y"
                {
			(yyval.n) = (yyvsp[0].n);
		}
#line 2002 "filter-parser.c"
    break;

  case 25: /* postfix_expression: postfix_expression DOT prefix_expression  */
#line 637 "filter-parser.y"
                {
			(yyval.n) = (yyvsp[0].n);
			(yyval.n)->u.expression.post_op = AST_LINK_DOT;
			(yyval.n)->u.expression.prev = (yyvsp[-2].n);
		}
#line 2012 "filter-parser.c"
    break;

  case 26: /* postfix_expression: postfix_expression RARROW prefix_expression  */
#line 643 "filter-parser.y"
                {
			(yyval.n) = (yyvsp[0].n);
			(yyval.n)->u.expression.post_op = AST_LINK_RARROW;
			(yyval.n)->u.expression.prev = (yyvsp[-2].n);
		}
#line 2022 "filter-parser.c"
    break;

  case 27: /* unary_expression: postfix_expression  */
#line 652 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2028 "filter-parser.c"
    break;

  case 28: /* unary_expression: primary_expression  */
#line 654 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2034 "filter-parser.c"
    break;

  case 29: /* unary_expression: unary_operator unary_expression  */
#line 656 "filter-parser.y"
                {
			(yyval.n) = (yyvsp[-1].n);
			(yyval.n)->u.unary_op.child = (yyvsp[0].n);
		}
#line 2043 "filter-parser.c"
    break;

  case 30: /* unary_operator: PLUS  */
#line 664 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_UNARY_OP);
			(yyval.n)->u.unary_op.type = AST_UNARY_PLUS;
		}
#line 2052 "filter-parser.c"
    break;

  case 31: /* unary_operator: MINUS  */
#line 669 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_UNARY_OP);
			(yyval.n)->u.unary_op.type = AST_UNARY_MINUS;
		}
#line 2061 "filter-parser.c"
    break;

  case 32: /* unary_operator: NOT_OP  */
#line 674 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_UNARY_OP);
			(yyval.n)->u.unary_op.type = AST_UNARY_NOT;
		}
#line 2070 "filter-parser.c"
    break;

  case 33: /* unary_operator: NOT_BIN  */
#line 679 "filter-parser.y"
                {
			(yyval.n) = make_node(parser_ctx, NODE_UNARY_OP);
			(yyval.n)->u.unary_op.type = AST_UNARY_BIT_NOT;
		}
#line 2079 "filter-parser.c"
    break;

  case 34: /* multiplicative_expression: unary_expression  */
#line 687 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2085 "filter-parser.c"
    break;

  case 35: /* multiplicative_expression: multiplicative_expression STAR unary_expression  */
#line 689 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_MUL, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2093 "filter-parser.c"
    break;

  case 36: /* multiplicative_expression: multiplicative_expression DIV_OP unary_expression  */
#line 693 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_DIV, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2101 "filter-parser.c"
    break;

  case 37: /* multiplicative_expression: multiplicative_expression MOD_OP unary_expression  */
#line 697 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_MOD, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2109 "filter-parser.c"
    break;

  case 38: /* additive_expression: multiplicative_expression  */
#line 704 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2115 "filter-parser.c"
    break;

  case 39: /* additive_expression: additive_expression PLUS multiplicative_expression  */
#line 706 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_PLUS, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2123 "filter-parser.c"
    break;

  case 40: /* additive_expression: additive_expression MINUS multiplicative_expression  */
#line 710 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_MINUS, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2131 "filter-parser.c"
    break;

  case 41: /* shift_expression: additive_expression  */
#line 717 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2137 "filter-parser.c"
    break;

  case 42: /* shift_expression: shift_expression LEFT_OP additive_expression  */
#line 719 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_BIT_LSHIFT, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2145 "filter-parser.c"
    break;

  case 43: /* shift_expression: shift_expression RIGHT_OP additive_expression  */
#line 723 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_BIT_RSHIFT, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2153 "filter-parser.c"
    break;

  case 44: /* and_expression: shift_expression  */
#line 730 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2159 "filter-parser.c"
    break;

  case 45: /* and_expression: and_expression AND_BIN shift_expression  */
#line 732 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_BIT_AND, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2167 "filter-parser.c"
    break;

  case 46: /* exclusive_or_expression: and_expression  */
#line 739 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2173 "filter-parser.c"
    break;

  case 47: /* exclusive_or_expression: exclusive_or_expression XOR_BIN and_expression  */
#line 741 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_BIT_XOR, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2181 "filter-parser.c"
    break;

  case 48: /* inclusive_or_expression: exclusive_or_expression  */
#line 748 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2187 "filter-parser.c"
    break;

  case 49: /* inclusive_or_expression: inclusive_or_expression OR_BIN exclusive_or_expression  */
#line 750 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_BIT_OR, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2195 "filter-parser.c"
    break;

  case 50: /* relational_expression: inclusive_or_expression  */
#line 757 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2201 "filter-parser.c"
    break;

  case 51: /* relational_expression: relational_expression LT_OP inclusive_or_expression  */
#line 759 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_LT, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2209 "filter-parser.c"
    break;

  case 52: /* relational_expression: relational_expression GT_OP inclusive_or_expression  */
#line 763 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_GT, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2217 "filter-parser.c"
    break;

  case 53: /* relational_expression: relational_expression LE_OP inclusive_or_expression  */
#line 767 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_LE, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2225 "filter-parser.c"
    break;

  case 54: /* relational_expression: relational_expression GE_OP inclusive_or_expression  */
#line 771 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_GE, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2233 "filter-parser.c"
    break;

  case 55: /* equality_expression: relational_expression  */
#line 778 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2239 "filter-parser.c"
    break;

  case 56: /* equality_expression: equality_expression EQ_OP relational_expression  */
#line 780 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_EQ, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2247 "filter-parser.c"
    break;

  case 57: /* equality_expression: equality_expression NE_OP relational_expression  */
#line 784 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_NE, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2255 "filter-parser.c"
    break;

  case 58: /* logical_and_expression: equality_expression  */
#line 791 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2261 "filter-parser.c"
    break;

  case 59: /* logical_and_expression: logical_and_expression AND_OP equality_expression  */
#line 793 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_AND, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2269 "filter-parser.c"
    break;

  case 60: /* logical_or_expression: logical_and_expression  */
#line 800 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2275 "filter-parser.c"
    break;

  case 61: /* logical_or_expression: logical_or_expression OR_OP logical_and_expression  */
#line 802 "filter-parser.y"
                {
			(yyval.n) = make_op_node(parser_ctx, AST_OP_OR, (yyvsp[-2].n), (yyvsp[0].n));
		}
#line 2283 "filter-parser.c"
    break;

  case 62: /* expression: logical_or_expression  */
#line 809 "filter-parser.y"
                {	(yyval.n) = (yyvsp[0].n);					}
#line 2289 "filter-parser.c"
    break;

  case 63: /* translation_unit: expression  */
#line 814 "filter-parser.y"
                {
			parser_ctx->ast->root.u.root.child = (yyvsp[0].n);
		}
#line 2297 "filter-parser.c"
    break;


#line 2301 "filter-parser.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (parser_ctx, scanner, YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, parser_ctx, scanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, parser_ctx, scanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (parser_ctx, scanner, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, parser_ctx, scanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, parser_ctx, scanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

